# Correlation, T.Tests, and ANOVA  --------------------------------------------------

# Load Libraries 
library(readr)
library(dplyr)
library(ggplot2)

# Load Data
data <- read_csv("drw_desc_read.csv")
data_long <- read_csv("grades_read_long.csv")


# Correlations ------------------------------------------------------------

# Research Question: Do SMOG and NDC scores correlate? 

# Quick Plot 
ggplot(data, aes(x=SMOG,y=NDC))+geom_point()

cor.test(data$SMOG,data$NDC)



# Research Question: Do NDC and FKR scores correlate? 
#insert code here 


# T Tests -----------------------------------------------------------------

# Research Question: Is there a statistically significant difference betweent the average grades awarded by Professors Boyle or Diab? 

# Quick Plot 
data_long %>%
  filter(Instructor=="Boyle"|Instructor=="Diab") %>%
  ggplot(aes(x=Instructor,y=grade)) + 
    geom_boxplot()

# Subset Data
boyle <- subset(data_long,Instructor=="Boyle")
diab <- subset(data_long,Instructor=="Diab")

#T Test
t.test(boyle$grade,diab$grade)

# Research Question: Is there a statistically significant difference betweent the average grades awarded by Professors Davis or Longaker? 
#Insert code here 


# Analyses of Variance ----------------------------------------------------
# Research Question: Is there a statistically significant difference betweent the average grades awarded by Professors Boyle, Davis, and Diab? 
# Quick Plot 
data_long %>%
  filter(Instructor=="Boyle"|Instructor=="Diab"|Instructor=="Davis") %>%
  ggplot(aes(x=Instructor,y=grade)) + 
  geom_boxplot()

data_long_bdd <- data_long %>% 
  filter(Instructor=="Boyle"|Instructor=="Diab"|Instructor=="Davis")


# Compute the analysis of variance
res.aov <- aov(grade ~ Instructor, data = data_long_bdd)

# Summary of the analysis
summary(res.aov)

#Tukey's HSD 
TukeyHSD(res.aov)

# Research Question: Is there a statistically significant difference between the average grades awarded in each type of course (E, C, D)? 
