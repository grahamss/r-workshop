
# Sentiment Analysis Workshop ---------------------------------------------


#Pre-Flight (Allocate RAM) | CHECK AVAILABLE RAM FIRST! 
options(java.parameters = "-Xmx4000m")

#load libraries 
library(RSentiment)
library(readr)
library(dplyr)
library(ggplot2)

#BASIC SENTIMENT ANALYSIS with RSentiment 
## Package description: Analyzes sentiment of a sentence in English and assigns a score to it. It can classify sentences to the following categories of sentiments:- Positive, Negative, very Positive, very negative,
## Neutral. For a vector of sentences, it counts the number of sentences in each
## category of sentiment. In calculating the score, negation and various degrees
## of adjectives are taken into consideration. It deals only with English sentences.

### Run each of the following sentiment calculations 
calculate_sentiment("This is the most wonderfully amazing class I have ever taken.")
calculate_sentiment("I hate R, everything about R, and I hope it dies in a fire.")
calculate_sentiment("This is not fun, but it's not not fun either.")

### Play around with your own sample sentences 
calculate_sentiment("YOUR SENTENCE HERE")



# Russian FB  -------------------------------------------------------------


#download and read data
download.file("http://sscottgraham.com/files/russiadata_sent.csv","russiadata_sent.csv")
data <- read_csv("russiadata_sent.csv")

#calculate sentiment | calculate_sentiment()

data$sent <- calculate_sentiment(data$AdText)

data %>% filter(sent$sentiment=="Very Positive") %>%
  select(AdText)

data %>% filter(sent$sentiment=="Very Negative") %>%
  select(AdText)


#calcuate score | calculate_sentiment()



#summaraize data 


#RQ: Is there a statistically significant relationship between sentiment and AdClicks? 


